/**
 * 
 */
package com.citi.training.employees.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

/**
 * @author Administrator
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTest {

	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;

	/**
	 * Transactional annotation insinuates that this is a transaction i.e. rollback
	 */
	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlEmployeeDao.create(new Employee(-1, "Frank", 10.0));

		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}
	
	/**
	 * Testing findById method
	 */
	@Test
	@Transactional
	public void test_findById() {
		mysqlEmployeeDao.create(new Employee(51, "Frank", 10.0));

		assertEquals(mysqlEmployeeDao.findById(51).getId(), 51);
	}
	
	/**
	 * findByIdException test
	 * @throws EmployeeNotFoundException
	 */
	@Test(expected = EmployeeNotFoundException.class)
	@Transactional
	public void test_findByIDException() throws EmployeeNotFoundException {
		mysqlEmployeeDao.findById(1);
	}

	@Test
	@Transactional
	public void test_deleteById() {
		mysqlEmployeeDao.create(new Employee(-1, "Frank", 10.0));
		mysqlEmployeeDao.create(new Employee(-1, "Sally", 10.0));
		mysqlEmployeeDao.deleteById(51);

		assertEquals(mysqlEmployeeDao.findAll().size(), 1);
	}

}
