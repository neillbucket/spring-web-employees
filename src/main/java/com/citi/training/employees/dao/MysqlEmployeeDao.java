/**
 * 
 */
package com.citi.training.employees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

/**
 * Component = a general bean but in Dao many people use the Repository
 * Annotation - at any stage you can search based on repository - dont worry too
 * much its just handy for searching for the Daos
 * 
 * @author Administrator
 *
 */
@Component
public class MysqlEmployeeDao implements EmployeeDao {

	@Autowired
	JdbcTemplate tpl;

	@Override
	public List<Employee> findAll() {

		return tpl.query("SELECT id, name, salary FROM employee", new EmployeeMapper());
	}

	/**
	 * Make an EmployeeMapper RowMapper interface allows to map a row of the
	 * relations with the instance of user-defined class. It iterates the ResultSet
	 * internally and adds it into the collection. So we don't need to write a lot
	 * of code to fetch the records as ResultSetExtractor.
	 * 
	 * @author Administrator
	 *
	 */
	private static final class EmployeeMapper implements RowMapper<Employee> {
		/**
		 * This Mapper creates employee objects based on the query
		 */
		@Override
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {

			return new Employee(rs.getInt("id"), rs.getString("name"), rs.getDouble("salary"));
		}

	}

	@Override
	public Employee findById(int id) {
		List<Employee> employees = tpl.query("SELECT id,  name, salary FROM employee WHERE id = ?", new Object[] { id },
				new EmployeeMapper());
		if(employees.size() <= 0) {
			throw new EmployeeNotFoundException("Employee with id =[" + id + "]not found");
		}
		return employees.get(0);
	}

	@Override
	public Employee create(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("insert into employee (name, salary) values (?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, employee.getName());
				ps.setDouble(2, employee.getSalary());
				return ps;
			}
		}, keyHolder);
		employee.setId(keyHolder.getKey().intValue());
		return employee;
	}

	@Override
	public void deleteById(int id) {
		//This method already throws an exception
		findById(id);
		tpl.update("DELETE FROM employee WHERE id = ?", id);

	}

}
